<!DOCTYPE html>
<html>
    <meta charset="utf-8"/>
    <head><title>register</title></head>
    <link rel="stylesheet" href="style.css"/>
<body>
<?php

    require('db.php');
    if (isset($_REQUEST['username']))
    {
        $username = stripslashes($_REQUEST['username']);
        $username = mysqli_real_escape_string($con,$username);
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con,$password);

        $email = stripslashes($_REQUEST['email']);
        $email = mysqli_real_escape_string($con,$email);
        $emailErr =0;
        $create_date = date("Y-m-d H:i:s");
        $result = "";
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $emailErr = 1;
        }
        else{
            $query = "INSERT into `usersinfo`(username,password,email,create_datetime)
                  VALUES ('$username','". md5($password)."','$email','$create_date')";
            $result = mysqli_query($con, $query);
        }
        if ($result)
        {
            echo "<div class='form'>
                  <h3>You are registered successfully.</h3><br/>
                  <p class='link'>Click here to <a href='login.php'>Login</a></p>
                  </div>";
        }
        else if($emailErr === 1)
        {
            echo "<div class='form'>
                  <h3>invalid email adress</h3><br/>
                  <p class='link'>Click here to <a href='registration.php'>registration</a> again.</p>
                  </div>";
        }
        else
        {
            echo "<div class='form'>
                  <h3>Required fields are missing.</h3><br/>
                  <p class='link'>Click here to <a href='registration.php'>registration</a> again.</p>
                  </div>";
        }
    }
    else
    {
?>
        <form class="form" action="" method="post">
            <h1 class="login-title">Registration</h1>
            <input type="text" class="login-input" name="username" placeholder="Username" required />
            <input type="text" class="login-input" name="email" placeholder="Email Adress" >
            <input type="password" class="login-input" name="password" placeholder="Password" >
            <input type="submit" name="submit" value="Register" class="login-button">
            <p class="link"><a href="login.php">Click to Login</a></p>
        </form>
<?php
    }
?>
</body>
</html>
